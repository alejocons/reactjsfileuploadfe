import React from 'react';
import Search from "./components/Search";
import { FilePond } from 'react-filepond';
import './App.css';	
import './Search.css';
import 'filepond/dist/filepond.min.css';

class App extends React.Component {
constructor(props) {
        super(props)
//		this.code = window.location.search.substring(1);
//		localStorage.clear();
		this.REST_API_URL_UPLOAD = process.env.REACT_APP_API_ENDPOINT + '/api/upload';
		this.codeAPP = this.getQueryVariableAPP('code');
//		console.log('code',this.codeAPP);
//		this.authenticationTokenAPP = localStorage.getItem('idToken');
//		console.log('authenticationToken',this.authenticationTokenAPP);		
        this.state = {
            wasClicked: false,
            selectedIndex: 0,
            selectedID: "",
			query: ``,
            files: [
                {
                    id: 0,
                    src: "#",
                    alt: "Placeholder Text",
                },
            ],
        }

    }
	handleInit() {
        console.log('FilePond instance has initialised', this.pond);
    }
	getQueryVariableAPP	(variable)
	{
			var query = window.location.search.substring(1);
//			console.log(query)//get all query strings
			var vars = query.split("&");
//			console.log(vars) //[ parse the string ]
			for (var i=0;i<vars.length;i++) {
						var pair = vars[i].split("=");
//						console.log(pair)
			if(pair[0] === variable){return pair[1];}
			 }
			 return(false);
	}
    handleOnClick(e) {
        const index = this.state.files.findIndex(i => i.id === e.target.id)
        this.setState({
            selectedIndex: index,
            wasClicked: true,
            selectedID: e.target.id,
        })
        setTimeout(() => {
            this.setState({ wasClicked: false })
        }, 500)
    }
	
	render() {
		return (
            <div className="App"> 
			<Search />
			<h2 className="heading">Upload Your Files Here.</h2>
                {/* Pass FilePond properties as attributes */}
				 <FilePond
                    // Set the callback here.
				    ref={ref => this.pond = ref}
                    onprocessfile={this.handlePondFile}
                    allowMultiple
                    maxFiles={1}
				    oninit={() => this.handleInit() }
                    name="file"
					server=
							{
								{
									url: this.REST_API_URL_UPLOAD,
									process: {
										headers: {
												'Authorization': 'Bearer ' +this.codeAPP,
												'xsrf-token': ' ' +localStorage.getItem('idToken')
										},
										 onload: (response) => { // Once response is received, pushed data to localCache
											if(localStorage.getItem('idToken') === null) {
												localStorage.setItem('idToken', JSON.parse(response).context.headers.idToken);
											}
										},
									}
								}
							}>
				</FilePond>

            </div>
			
		);
	}
}

export default App;
