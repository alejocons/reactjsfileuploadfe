import React from 'react';
import '../Search.css';
import axios from 'axios';
import Loader from '../loader.gif';
import PdfImage from '../pdfImage.jpg';
import PageNavigation from './PageNavigation';

const REST_API_URL_SEARCH = process.env.REACT_APP_API_ENDPOINT + '/api/search';
const REST_API_URL_DOWNLOAD= process.env.REACT_APP_API_ENDPOINT + '/api/download';

class Search extends React.Component {

	constructor( props ) {
		super( props );
		this.code = 'Bearer '.concat(this.getQueryVariable('code'));
//		console.log('code',this.code)
		this.state = {
			query: '',
			results: {},
			loading: false,
			message: '',
			totalResults: 0,
			totalPages: 0,
			currentPageNo: 0,
		};

		this.cancel = '';
	}


	/**
	 * Get the Total Pages count.
	 *
	 * @param total
	 * @param denominator Count of results per page
	 * @return {number}
	 */
	getPageCount = ( total, denominator ) => {
		const divisible	= 0 === total % denominator;
		const valueToBeAdded = divisible ? 0 : 1;
		return Math.floor( total/denominator ) + valueToBeAdded;
	};

	/**
	 * Fetch the search results and update the state with the result.
	 * Also cancels the previous query before making the new one.
	 *
	 * @param {int} updatedPageNo Updated Page No.
	 * @param {String} query Search Query.
	 *
	 */
	fetchSearchResults = ( updatedPageNo = '', query) => {
		const pageNumber = updatedPageNo ? `&page=${updatedPageNo}` : '';
		const searchUrl = REST_API_URL_SEARCH+`/?keyword=${query}${pageNumber}`;

		if( this.cancel ) {
			this.cancel.cancel();
		}

		this.cancel = axios.CancelToken.source();

		axios.get( searchUrl, { headers: { Authorization: this.code, 'xsrf-token': ' ' +localStorage.getItem('idToken') } },
			{
				cancelToken: this.cancel.token
			}
		)
			.then( res => {
				if(localStorage.getItem('idToken') === null) {
					localStorage.setItem('idToken', res.data.idToken);
				}
				const total = res.data.totalResultSet;
				const totalPagesCount = this.getPageCount( total, 20 );
				const resultNotFoundMsg = ! res.data.documentsList.length
										? 'There are no more search results. Please try a new search'
										: '';
				this.setState( {
					results: res.data.documentsList,
					message: resultNotFoundMsg,
					totalResults: total,
					totalPages: totalPagesCount,
					currentPageNo: updatedPageNo,
					loading: false
				} )
			} )
			.catch( error => {
				if ( axios.isCancel(error) || error ) {
					this.setState({
						loading: false,
						message: 'Failed to fetch the data. Please check network'	
					})
				}
			} )
	};

	handleOnInputChange = (event) => {
		const query = event.target.value;
		if ( ! query || query.length < 3) {
			this.setState( { query, results: {}, message: '', totalPages: 0, totalResults: 0 } );
		} else {
			this.setState( { query, loading: true, message: '' }, () => {
				this.fetchSearchResults( 1, query );
			} );
		}
	};
	
	handleOnKeyDown = ( event ) => {
		if (event.key === "Enter") {
			const query = event.target.value;
			if ( ! query || query.length < 3) {
				this.setState( { query, results: {}, message: '', totalPages: 0, totalResults: 0 } );
			} else {
				this.setState( { query, loading: true, message: '' }, () => {
					this.fetchSearchResults( 1, query );
				} );
			}
		}
	};

    extractFileName = (contentDispositionValue) => {
//		alert('hello' +contentDispositionValue);
         var filename = "";
         if (contentDispositionValue && contentDispositionValue.indexOf('attachment') !== -1) {
             var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
             var matches = filenameRegex.exec(contentDispositionValue);
             if (matches != null && matches[1]) {
                 filename = matches[1].replace(/['"]/g, '');
             }
         }
         return filename;
    };
	downloadFile = ( key ) => {
		axios({
		  url: REST_API_URL_DOWNLOAD+'?key='+key,
		  method: 'GET',
		  responseType: 'blob', // important
		  headers: { Authorization: this.code, 'xsrf-token': ' ' +localStorage.getItem('idToken') } ,
		})
		.then((response) => {
//		   console.log('hearders',response.headers);
		   const filename=this.extractFileName(response.headers['content-disposition']);
		   const url = window.URL.createObjectURL(new Blob([response.data]));
		   const link = document.createElement('a');
		   link.href = url;
		   link.setAttribute('download', filename); //or any other extension
		   document.body.appendChild(link);
		   link.click();
		   link.remove();
		});	  
	};	
	getQueryVariable(variable)
	{
			var query = window.location.search.substring(1);
//			console.log(query)//get all query strings
			var vars = query.split("&");
//			console.log(vars) //[ parse the string ]
			for (var i=0;i<vars.length;i++) {
						var pair = vars[i].split("=");
//						console.log(pair)
			if(pair[0] === variable){return pair[1];}
			 }
			 return(false);
	};
	
	/**
	 * Fetch results according to the prev or next page requests.
	 *
	 * @param {String} type 'prev' or 'next'
	 */
	handlePageClick = ( type ) => {
		this.preventDefault();
		const updatePageNo = 'prev' === type
			? this.state.currentPageNo - 1
			: this.state.currentPageNo + 1;

		if( ! this.state.loading  ) {
			this.setState( { loading: true, message: '' }, () => {
				this.fetchSearchResults( updatePageNo, this.state.query );
			} );
		}
	};

	renderSearchResults = () => {
		const { results } = this.state;

		if ( Object.keys( results ).length && results.length ) {
			return (
				<div className="results-container">
					{ results.map( result => {
						return (							
							<button className="result-item" onClick={() => this.downloadFile(`${result.key}`)}>
								<h6 className="image-username">{result.fileName}</h6>
								<div className="image-wrapper">
									<img className="image" src={ PdfImage } alt={`${result.fileName}`}/>
								</div>
							</button>
							)
					} ) }

				</div>
			)
		}
	};

	render() {
		const { query, loading, message, currentPageNo, totalPages } = this.state;

		const showPrevLink = 1 < currentPageNo;
		const showNextLink = totalPages > currentPageNo;

		return (
			<div className="container">
			{/* Search Input*/}
			<label className="search-label" htmlFor="search-input">
				<input
					type="text"
					name="query"
					defaultValue={ query }
					id="search-input"
					placeholder="Search..."
					onKeyDown={this.handleOnKeyDown}
				/>
				<i className="fa fa-search search-icon" aria-hidden="true"/>
			</label>

			{/*	Error Message*/}
				{message && <p className="message">{ message }</p>}

			{/*	Loader*/}
			<img src={ Loader } className={`search-loading ${ loading ? 'show' : 'hide' }`} alt="loader"/>

			{/*Navigation*/}
			<PageNavigation
				loading={loading}
				showPrevLink={showPrevLink}
				showNextLink={showNextLink}
				handlePrevClick={ () => this.handlePageClick('prev', this )}
				handleNextClick={ () => this.handlePageClick('next', this )}
			/>

			{/*	Result*/}
			{ this.renderSearchResults() }

			{/*Navigation*/}
			<PageNavigation
				loading={loading}
				showPrevLink={showPrevLink}
				showNextLink={showNextLink}
				handlePrevClick={ () => this.handlePageClick('prev', this )}
				handleNextClick={ () => this.handlePageClick('next', this )}
			/>

			</div>
		)
	}
}

export default Search